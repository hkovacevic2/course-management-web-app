const express = require('express');
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser');

const app = express();
const port = process.env.PORT || 3000;
const db = require('./models/db.js');

app.use(express.static("public"));
app.use(express.static("public/html"));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.text());

app.get('/vjezbe/', function(req, res) {
    db.vjezba.findAll().then(function(vjezbe) {
        var listaZadataka = [];
        vjezbe.every(vjezba => listaZadataka.push(vjezba.countZadaci()));
        Promise.all(listaZadataka).then(function(zadaci) {
            res.status(200).json(JSON.parse(`{ "brojVjezbi": ${vjezbe.length}, "brojZadataka": ${JSON.stringify(zadaci)} }`));
        });
    });
});

app.post('/vjezbe', function(req, res) {
    var pogresno = "";
    var brVjezbi = req.body.brojVjezbi;
    var brZadataka = req.body.brojZadataka;
    if (!(brVjezbi == null && brZadataka == null)) {
        if (brVjezbi < 1 || brVjezbi > 15 || isNaN(brVjezbi)) pogresno += "brojVjezbi";
        for (var i = 0; i < brZadataka.length; ++i) {
            if (isNaN(brZadataka[i]) || !(brZadataka[i] >= 0 && brZadataka[i] <= 10)) {
                if (pogresno !== "") pogresno += `,z${i}`;
                else pogresno += `z${i}`;
            }
        }
        if (brVjezbi !== brZadataka.length) {
            if (pogresno !== "") pogresno += ",brojZadataka";
            else pogresno += "brojZadataka";
        }
    } else {
        if (brVjezbi == null) pogresno += "brojVjezbi";
        if (brZadataka == null) {
            if (pogresno !== "") pogresno += ",brojZadataka";
            else pogresno += "brojZadataka";
        }
    }

    if (pogresno !== "") res.status(400).json(JSON.parse(`{ "status": "error", "data": "Pogrešan parametar ${pogresno}"}`));
    else {
        db.zadatak.destroy({ truncate: { cascade: true } }).then(() => {
            db.vjezba.destroy({ truncate: { cascade: true } }).then(() => {
                var listaVjezbi = [];
                var listaZadataka = [];
                for (let i = 1; i <= brVjezbi; ++i) {
                    listaVjezbi.push(db.vjezba.create({ naziv: `Vježba ${i}` }).then(function(vjezba) {
                        listaZadataka = [];
                        for (let j = 1; j <= brZadataka[i - 1]; ++j) {
                            listaZadataka.push(db.zadatak.create({ naziv: `Zadatak ${j}`, vjezbaId: vjezba.id }));
                        }
                        Promise.all(listaZadataka).then(() => { return new Promise(function(resolve, reject) { resolve(vjezba); }) });
                    }));
                }
                Promise.all(listaVjezbi).then(function(vjezbe) {
                    res.status(200).json({ "brojVjezbi": brVjezbi, "brojZadataka": brZadataka });
                });
            })
        })
    }
});

app.post('/student', function(req, res) {
    var student = req.body;
    db.student.findOrCreate({ where: { ime: student.ime, prezime: student.prezime, index: student.index, grupa: student.grupa } }).then((result) => {
        if (result[1]) {
            db.grupa.findOrCreate({ where: { naziv: student.grupa } }).then((rez) => {
                result[0].set({ grupaId: rez[0].id });
                result[0].save().then(() => {
                    res.status(200).json(JSON.parse(`{ "status": "Kreiran student!"}`));
                });
            })
        } else {
            res.status(400).json(JSON.parse(`{ "status": "Student sa indexom ${student.index} već postoji!"}`));
        }
    });
});

app.put('/student/:index', function(req, res) {
    db.student.findOne({ where: { index: req.params.index } }).then((student) => {
        if (student) {
            db.grupa.findOrCreate({ where: { naziv: req.body.grupa } }).then((rez) => {
                student.set({ grupa: req.body.grupa, grupaId: rez[0].id });
                student.save().then(() => {
                    res.status(200).json(JSON.parse(`{ "status": "Promjenjena grupa studentu ${req.params.index}"}`));
                });
            });
        } else {
            res.status(400).json(JSON.parse(`{ "status": "Student sa indexom ${req.params.index} ne postoji!"}`));
        }
    });
});

app.post('/batch/student', function(req, res) {
    var redovi = req.body.split(/[\r\n]/g);
    var studenti = [];
    var listaStudenata = [];
    for (let i = 0; i < redovi.length; ++i) {
        let attr = redovi[i].split(',');
        if (attr.length != 4) continue;
        let student = {};
        student.ime = attr[0];
        student.prezime = attr[1];
        student.index = attr[2];
        student.grupa = attr[3];
        studenti.push(student);
    }
    studenti.forEach((student) => {
        listaStudenata.push(db.student.findOrCreate({ where: { ime: student.ime, prezime: student.prezime, index: student.index, grupa: student.grupa } }).then((result) => {
            if (result[1]) {
                db.grupa.findOrCreate({ where: { naziv: student.grupa } }).then((rez) => {
                    result[0].set({ grupaId: rez[0].id });
                    result[0].save().then(() => {
                        return new Promise((resolve, reject) => resolve(student));
                    });
                })
            } else {
                return new Promise((resolve, reject) => reject(student));
            }
        }));
    });
    var br = 0;
    var indexi = "";
    Promise.allSettled(listaStudenata).then((results) => {
        results.forEach(r => {
            if (r.status === 'rejected') {
                br++;
                indexi += `${r.reason.index},`;
            }
        });
        indexi = indexi.slice(0, -1);
        if (br == 0) {
            res.status(200).json(JSON.parse(`{ "status": "Dodano ${results.length} studenata!"}`));
        } else {
            res.status(200).json(JSON.parse(`{ "status": "Dodano ${results.length-br} studenata, a studenti ${indexi} već postoje!"}`));
        }
    });
});

db.sequelize.sync({ force: false }).then(() => {
    console.log("Gotovo kreiranje tabela!");
    app.listen(port, () => console.log('Server started at http://localhost:' + port));
});

module.exports = app;