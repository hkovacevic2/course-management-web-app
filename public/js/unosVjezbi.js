function inputPolja(dom, br) {
    VjezbeAjax.dodajInputPolja(dom, br);
}

function parseForme() {
    var brVjezbi = parseInt(document.getElementById('brojVjezbi').value);
    var zadaci = document.getElementById('unosZadataka').querySelectorAll('input');
    let brZadataka = [];
    for (var i = 0; i < zadaci.length; ++i) brZadataka.push(parseInt(zadaci[i].value));
    VjezbeAjax.posaljiPodatke({ "brojVjezbi": brVjezbi, "brojZadataka": brZadataka }, (error, data) => {
        if (error == null) console.log("Uspješno poslani podaci: " + data);
        else console.log("Došlo je do greške prilikom slanja: " + error);
    });
}