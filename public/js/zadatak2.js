let server = require('../../index.js')
const Sequelize = require('sequelize');
const db = require('../../models/db.js')

let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);
chai.should();
let assert = chai.assert;

function preTest() {
    lista = [];
    return new Promise(function(resolve) {
        lista.push(db.grupa.create({ nazivGrupe: "G1" }));
        lista.push(db.grupa.create({ nazivGrupe: "G2" }));
        lista.push(db.student.create({ ime: "Hamza", prezime: "Kovačević", index: "18738", grupa: "G1" }).then((student) => {
            db.grupa.findOne({ where: { nazivGrupe: "G1" } }).then((grupa) => {
                grupa.addStudentiGrupe(student);
            })
        }));
        Promise.all(lista).then(() => {
            resolve();
        })
    })
}
describe("Test GET /vjezbe", function() {
    this.beforeAll(function(done) {
        db.sequelize.sync({ force: true }).then(function() {
            done();
        });
    })
    this.afterAll(function(done) {
        db.sequelize.sync({ force: true }).then(function() {
            done();
        });
    })
    it('prazna baza, dodavanje studenta', function(done) {
        chai.request(server).post('/student')
            .set('content-type', 'application/json')
            .send({ ime: "Hare", prezime: "Hare", index: "18703", grupa: "TestGrupa" })
            .end(function(err, res) {
                res.should.have.status(200);
                done();
            })
    })
    it('dodavanje studenta sa istim indexom', function(done) {
        //ponovno dodavanje studenta istog studenta kao u prvom testu  
        chai.request(server).post('/student')
            .set('content-type', 'application/json')
            .send({ ime: "ime", prezime: "prezime", index: "18703", grupa: "TestGrupa" })
            .end(function(err, res) {
                res.should.have.status(200);
                res.body.status.should.eql("Student sa indexom 18703 već postoji!");
                done();
            })
    })
})

describe('Test PUT /student/:index', function() {
    this.beforeAll(function(done) {
        db.sequelize.sync({ force: true }).then(function() {
            preTest().then(() => {
                done();
            })
        });
    })
    this.afterAll(function() {
        db.sequelize.sync({ force: true }).then(function() {
            done();
        });
    })
    it("Mijenjanje grupe studenta sa indexom 18703 u postojecu grupu", function(done) {
        chai.request(server).put('/student/18703')
            .set('content-type', 'application/json')
            .send({ grupa: "G2" })
            .end(function(err, res) {
                res.should.have.status(200);
                res.body.status.should.eql("Promjenjena grupa studentu 18703");
                done();
            })
    })
    it('Vracanje prvobitne grupe studentu sa indexom 18703', function(done) {
        chai.request(server).put('/student/18703')
            .set('content-type', 'application/json')
            .send({ grupa: "G1" })
            .end(function(err, res) {
                res.should.have.status(200);
                res.body.status.should.eql("Promjenjena grupa studentu 18703");
                done();
            })
    })
    it('Postavljanje grupe koja ne postoji', function(done) {
        chai.request(server).put('/student/18703')
            .set('content-type', 'application/json')
            .send({ grupa: "G3" })
            .end(function(err, res) {
                db.grupa.findAll().then((grupe) => {
                    grupe.should.have.lengthOf(3, "U bazi se nalaze 3 grupe");
                    res.should.have.status(200);
                    res.body.status.should.eql("Promjenjena grupa studentu 18703");

                }).then(() => { done() })
            })
    })
    it('Mijenanje grupe nepostojecem studentu', function(done) {
        chai.request(server).put('/student/11111')
            .set('content-type', 'application/json')
            .send({ grupa: "G2" })
            .end(function(err, res) {
                res.should.have.status(404);
                res.body.status.should.eql("Student sa indexom 11111 ne postoji");
                done();
            })
    })
})

describe("Test POST /batch/student", function() {
    this.beforeAll(function(done) {
        db.sequelize.sync({ force: true }).then(function() {
            preTest().then(() => {
                done();
            })
        });
    })
    it('Dodavanje samo jednog studenta', function(done) {
        chai.request(server).post("/batch/student").set('content-type', 'application/json')
            .send({ csv: "mujo,mujic,11223,grupa" })
            .end(function(err, res) {
                res.should.have.status(200);
                res.body.status.should.eql("Dodano 1 studenata!");
                done();
            })
    })
    it('Dodavanje vise studenata', function(done) {
        chai.request(server).post("/batch/student").set('content-type', 'application/json')
            .send({ csv: "mujo,drugi,11222,grupa\nfata,fatic,11224,grupaB\nfata,mujic,11225,grupaB" })
            .end(function(err, res) {
                res.should.have.status(200);
                res.body.status.should.eql("Dodano 3 studenata!");
                done();
            })
    })
    it('Dodavanje postojecih studenata', function(done) {
        chai.request(server).post("/batch/student").set('content-type', 'application/json')
            .send({ csv: "mujo,drugi,11222,grupa\nfata,fatic,11224,grupaB\nfata,mujic,11225,grupaB" })
            .end(function(err, res) {
                db.student.findAll().then((studenti) => {
                    studenti.should.have.lengthOf(5)
                    res.should.have.status(200);
                    //res.body.status.should.eql("Dodano 0 studenata, a studenti {11222,11224,11225} već postoje!");
                }).then(() => {
                    done();
                })
            })
    })
})