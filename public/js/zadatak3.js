let assert = chai.assert;
let should = chai.should();
let DOMpomocnogDIVa = document.getElementById('pomocniDiv');

describe('VjezbeAjax', function() {
    describe('dodajInputPolja()', function() {
        afterEach(function() {
            DOMpomocnogDIVa.innerHTML = '';
        });
        it('5 input polja', function() {
            VjezbeAjax.dodajInputPolja(DOMpomocnogDIVa, 5);
            assert.equal(DOMpomocnogDIVa.querySelectorAll('input').length, 5);
        });
        it('Ispravnost početne vrijednosti', function() {
            VjezbeAjax.dodajInputPolja(DOMpomocnogDIVa, 5);
            let inputs = DOMpomocnogDIVa.querySelectorAll('input');
            for (var i = 0; i < inputs.length; ++i) {
                assert.equal(inputs[i].value, 4);
            }
        });
        it('Ispravnost id-a i naziva', function() {
            VjezbeAjax.dodajInputPolja(DOMpomocnogDIVa, 5);
            let inputs = DOMpomocnogDIVa.querySelectorAll('input');
            for (var i = 0; i < inputs.length; ++i) {
                assert.equal(inputs[i].id, `z${i}`);
                assert.equal(inputs[i].name, `z${i}`);
            }
        });
    });
    describe('posaljiPodatke()', function() {
        beforeEach(function() {
            this.xhr = sinon.useFakeXMLHttpRequest();

            this.requests = [];
            this.xhr.onCreate = function(xhr) {
                this.requests.push(xhr);
            }.bind(this);
        });

        afterEach(function() {
            this.xhr.restore();
        });
        it('Ispravni podaci', function(done) {
            let obj = { "brojVjezbi": 3, "brojZadataka": [4, 4, 4] };
            VjezbeAjax.posaljiPodatke(obj, function(err, data) {
                assert.equal(err, null, 'Podaci su ispravni!');
                assert.deepEqual(data, JSON.stringify(obj));
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(obj));
            this.requests[0].respond(200, { "Content-Type": "text/json" }, JSON.stringify(obj));
        });
        it('Neispravni podaci', function(done) {
            let obj = { "brojVjezbi": 100, "brojZadataka": [4, 4, 4] };
            VjezbeAjax.posaljiPodatke(obj, function(err, data) {
                assert.equal(err, "Bad Request");
                assert.isNull(data);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(obj));
            this.requests[0].respond(400, { "Content-Type": "text/json" }, JSON.stringify(obj));
        })
    });
    describe('dohvatiPodatke()', function() {
        beforeEach(function() {
            this.xhr = sinon.useFakeXMLHttpRequest();

            this.requests = [];
            this.xhr.onCreate = function(xhr) {
                this.requests.push(xhr);
            }.bind(this);
        });

        afterEach(function() {
            this.xhr.restore();
        });
        it('Citanje uspjesno', function(done) {
            let obj = { "brojVjezbi": 5, "brojZadataka": [3, 4, 5, 6, 7] };
            VjezbeAjax.dohvatiPodatke(function(err, data) {
                assert.isNull(err);
                assert.equal(data, JSON.stringify(obj));
                done();
            });

            this.requests[0].respond(200, { "Content-Type": "text/json" }, JSON.stringify(obj));
        });
        it('Citanje neuspjesno', function(done) {
            let obj = { "error": "Bad Request" };
            VjezbeAjax.dohvatiPodatke(function(err, data) {
                assert.equal(err, "Bad Request");
                assert.isNull(data);
                done();
            });

            this.requests[0].respond(400, { "Content-Type": "text/json" }, JSON.stringify(obj));
        });
    });
    describe('iscrtajVjezbe()', function() {
        afterEach(function() {
            DOMpomocnogDIVa.innerHTML = '';
        });
        it('5 vjezbi', function() {
            let obj = { "brojVjezbi": 5, "brojZadataka": [1, 2, 3, 4, 5] };
            VjezbeAjax.iscrtajVjezbe(DOMpomocnogDIVa, obj);
            assert.equal(DOMpomocnogDIVa.children.length, 5);
        });
    });
    describe('iscrtajZadatke()', function() {
        afterEach(function() {
            DOMpomocnogDIVa.innerHTML = '';
        });
        it('1 zadatak', function() {
            let obj = { "brojVjezbi": 5, "brojZadataka": [1, 2, 3, 4, 5] };
            VjezbeAjax.iscrtajVjezbe(DOMpomocnogDIVa, obj);
            VjezbeAjax.iscrtajZadatke(DOMpomocnogDIVa.querySelector('div'), 1);
            assert.equal(DOMpomocnogDIVa.querySelector('div').childNodes.length, 2);
        });
        it('Da li se ispravno skrivaju zadaci 1', function() {
            let obj = { "brojVjezbi": 5, "brojZadataka": [1, 2, 3, 4, 5] };
            VjezbeAjax.iscrtajVjezbe(DOMpomocnogDIVa, obj);
            let v1 = DOMpomocnogDIVa.querySelector('#v1');
            let v2 = DOMpomocnogDIVa.querySelector('#v2');
            VjezbeAjax.iscrtajZadatke(v1, 1);
            assert.equal(v1.querySelector('div.box-2').childNodes.length, 1);
            VjezbeAjax.iscrtajZadatke(v2, 2);
            assert.equal(v1.querySelector('div.box-2').style.visibility, "hidden");
            assert.equal(v2.querySelector('div.box-2').childNodes.length, 2);
        });
        it('Da li se ispravno skrivaju zadaci 2', function() {
            let obj = { "brojVjezbi": 5, "brojZadataka": [1, 2, 3, 4, 5] };
            VjezbeAjax.iscrtajVjezbe(DOMpomocnogDIVa, obj);
            let v1 = DOMpomocnogDIVa.querySelector('#v1');
            VjezbeAjax.iscrtajZadatke(v1, 1);
            assert.equal(v1.querySelector('div.box-2').childNodes.length, 1);
            VjezbeAjax.iscrtajZadatke(v1, 1);
            assert.equal(v1.querySelector('div.box-2').style.visibility, "hidden");
        });
    });
});