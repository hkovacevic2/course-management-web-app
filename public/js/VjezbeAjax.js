let VjezbeAjax = (function() {
    const dodajInputPolja = function(DOMelementDIVauFormi, brojVjezbi) {
        DOMelementDIVauFormi.innerHTML = '';
        for (var i = 0; i < brojVjezbi; i++) {
            var labela = document.createElement('label');
            labela.appendChild(document.createTextNode(`Vježba ${i+1}:`));
            var input = document.createElement('input');
            input.setAttribute('id', `z${i}`);
            input.setAttribute('name', `z${i}`);
            input.setAttribute('type', 'number');
            input.setAttribute('value', 4);
            input.setAttribute('min', 0);
            input.setAttribute('max', 10);
            DOMelementDIVauFormi.appendChild(labela);
            DOMelementDIVauFormi.appendChild(document.createElement('br'));
            DOMelementDIVauFormi.appendChild(input);
            DOMelementDIVauFormi.appendChild(document.createElement('br'));
        }
    }
    const posaljiPodatke = function(vjezbeObjekat, callbackFja) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200)
                callbackFja(null, ajax.responseText);
            else if (ajax.readyState == 4)
                callbackFja(ajax.statusText, null);
        }
        ajax.open("POST", "http://localhost:3000/vjezbe", true);
        ajax.setRequestHeader('Content-Type', 'application/json');
        ajax.send(JSON.stringify(vjezbeObjekat));
    }
    const dohvatiPodatke = function(callbackFja) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200)
                callbackFja(null, ajax.responseText);
            else if (ajax.readyState == 4)
                callbackFja(ajax.statusText, null);
        }
        ajax.open("GET", "http://localhost:3000/vjezbe", true);
        ajax.send();
    }
    const iscrtajVjezbe = function(divDOMelement, vjezbeObjekat) {
        for (var i = 1; i <= vjezbeObjekat.brojVjezbi; ++i) {
            let novaVjezba = document.createElement('div');
            novaVjezba.classList.add('container');
            novaVjezba.setAttribute('id', `v${i}`);
            let brZadataka = vjezbeObjekat.brojZadataka[i - 1];
            novaVjezba.onclick = function() {
                iscrtajZadatke(novaVjezba, brZadataka);
            };
            let divNaziv = document.createElement('div');
            divNaziv.classList.add('box-1');
            divNaziv.appendChild(document.createTextNode(`Vježba ${i}`));
            novaVjezba.appendChild(divNaziv);
            divDOMelement.appendChild(novaVjezba);
        }
    }
    const iscrtajZadatke = function(vjezbaDOMelement, brojZadataka) {
        Array.from(vjezbaDOMelement.parentNode.children).filter(o => o !== vjezbaDOMelement).map(c => c.querySelector('div.box-2')).filter(o => o).forEach(o => o.style.visibility = "hidden");
        divZadataka = vjezbaDOMelement.querySelector('div.box-2');
        if (divZadataka == null) {
            divZadataka = document.createElement('div');
            divZadataka.classList.add('box-2');
            for (var i = 1; i <= brojZadataka; ++i) {
                let noviZadatak = document.createElement('div');
                noviZadatak.classList.add('box-3');
                noviZadatak.appendChild(document.createTextNode(`Zadatak ${i}`));
                divZadataka.appendChild(noviZadatak);
            }
            vjezbaDOMelement.appendChild(divZadataka);
        } else {
            if (divZadataka.style.visibility === "hidden") {
                divZadataka.style.visibility = "";
            } else divZadataka.style.visibility = "hidden";
        }
    }
    return {
        dodajInputPolja: dodajInputPolja,
        posaljiPodatke: posaljiPodatke,
        dohvatiPodatke: dohvatiPodatke,
        iscrtajVjezbe: iscrtajVjezbe,
        iscrtajZadatke: iscrtajZadatke
    }
}());